package main

import (
	"encoding/csv"
	"errors"
	"io"
	"os"
	"strconv"
)

func main() {
	file := "./ИТ управления предприятием-Мегатест-оценки.csv"
	goodGrade := 6.0

	f, err := os.Open(file)
	if err != nil {
		// return nil, err
	}
	defer f.Close()

	reader := csv.NewReader(f)
	reader.Comma = ','
	user2goodGradesNumber := map[string]int{}
	for {
		row, err := reader.Read()

		if err != nil {
			if err == io.EOF {
				break
			}
		}

		// println(len(row))

		updatedMap, err := updateUser2goodGradesNumberMap(row, goodGrade, user2goodGradesNumber)
		if err != nil {
		} else {
			user2goodGradesNumber = updatedMap
		}

	}

	for k, v := range user2goodGradesNumber {
		println(k, v)
	}
}

func updateUser2goodGradesNumberMap(row []string, goodGrade float64, user2goodGradesNumber map[string]int) (map[string]int, error) {
	if len(row) == 0 {
		return nil, errors.New("row is empty")
	}
	surname := row[0]
	name := row[1]
	grade, err := strconv.ParseFloat(row[6], 32)
	if err != nil {
		gradeInt, err := strconv.ParseInt(row[6], 10, 32)
		if err != nil {
			return nil, err
		}
		grade = float64(gradeInt)
	}

	if grade >= goodGrade {
		if _, ok := user2goodGradesNumber[surname+" "+name]; ok {
			user2goodGradesNumber[surname+" "+name] += 1
		} else {
			user2goodGradesNumber[surname+" "+name] = 1
		}
	}

	return user2goodGradesNumber, nil

}
